---
layout: gallery.njk
title:  Kittens For You
pagination:
  data: collections.cats
  size: 12
permalink: "gallery/{% if pagination.pageNumber > 0 %}{{ pagination.pageNumber | plus: 1  }}/{% endif %}index.html"
---

{% article_hero %}
    <div class="spacer spacer-40"></div>
    <h1>{{title}}</h1>
    <p class="breadcrumbs">{{page.url | filterCrubms}}<span class="orange">Cats</span></p>
    <div class="spacer spacer-40"></div>
{% endarticle_hero %}

{% assign tagName = "Breed"%}
{% include "partials/filter.njk" %}

<div class="gallery-main">
    <section>
        {% max_width %}
            {% responsive_container %}
            {%- for cat in pagination.items -%}
                {% container "100_50_33_33_33" %}
                    {% bigCard cat.data.heroPhoto, cat.data.catName, cat.data.catName, cat.data.catInfo.sex, cat.data.catInfo.date-of-birth, cat.data.catInfo.breed, cat.data.catPrice, cat.data.page.url %}
                {% end_container %}
            {%- endfor -%}
            {% endresponsive_container %}
        {% end_max_width %}
    </section>
</div>

<div class="pagination text-center">
    <section>
        <div class="spacer spacer-20"></div>
        {% max_width %}
            {%- for item in pagination.hrefs -%}
            <a href={{item}} aria-label="to page {{forloop.index}}" class="pagination-link{% if page.url == item %} current {% endif %}">
                {{ forloop.index }}
            </a>
            {%- endfor -%}
        {% end_max_width %}
        <div class="spacer spacer-90"></div>
    </section>
</div>