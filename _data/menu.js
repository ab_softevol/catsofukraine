const responsive = require('./responsive.json');

module.exports = {
    links : {
        "Home": "/",
        "Gallery": "/gallery/",
        "Articles": "/articles/",
        "News": "/news/",
        "Contact Us": "/contacts/"
    },
    breakpoint: responsive.MEDIA.laptop
}