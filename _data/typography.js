const responsive = require('./responsive.json');
const colors = require('./colors.json');

const BACKGROUND_COLOR = colors['Background'].color;
const ON_BACKGROUND = colors['On Background'].color;

const GAP = responsive.GAP;
const MAIN_FONT_FAMILY = 'Nunito';
const FMODS_FONT_FAMILY = 'Arial';
const FONT_FAMILY_STYLE = 'sans-serif';

module.exports = {
  body: {
    'default': {
      'margin' : '0',
      'padding' : '0',
      'color' : `${ON_BACKGROUND}`,
      'background-color': `${BACKGROUND_COLOR}`,
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`
    }
  },
  h1: {
    'default': {
      'margin' : '0',
      'padding' : `${GAP}px`,
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-size': '46px',
      'line-height': '63px',
      'font-weight': '700',
    },
    "phone" : {
      'font-size': '24px',
      'line-height': '33px',
    }    
  },
  h2: {
    'default': {
      'margin' : '0',
      'padding' : `${GAP}px`,
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-size': '34px',
      'line-height': '46px',
      'font-weight': '700',
    },
    "phone" : {
      'font-size': '18px',
      'line-height': '25px',
    }  
  },
  h3: {
    'default': {
      'margin' : '0',
      'padding' : `${GAP}px`,
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-size': '24px',
      'line-height': '33px',
      'font-weight': '700',
    },
    "phone" : {
      'font-size': '16px',
      'line-height': '22px',
    }  
  },
  p: {
    'default': {
      'margin' : '0',
      'padding' : `${GAP}px`,
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-size': '16px',
      'line-height': '22px',
      'font-weight': '400'
    },
    "phone" : {
      'font-size': '14px',
      'line-height': '19px',
    }  
  },
  "time": {
    'default': {
      'margin' : '0',
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-size': '16px',
      'line-height': '22px',
      'font-weight': '400'
    }
  },
  ".title": {
    'default': {
      'margin' : '0',
      'padding' : `${GAP}px`,
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-size': '18px',
      'line-height': '25px',
      'font-weight': '400'
    },
    "phone" : {
      'font-size': '16px',
      'line-height': '22px',
    } 
  },
  ".main-menu__link": {
    'default': {
      'padding' : `${GAP}px`,
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-weight': '400',
      'font-size': '16px',
      'line-height': '22px',
      'text-decoration': 'none',
    }
  },
  ".footer__link": {
    'default': {
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-weight': '400',
      'font-size': '16px',
      'line-height': '22px',
      'text-decoration': 'none',
    }
  },
  ".button-colored": {
    'default': {
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-weight': '700',
      'font-size': '14px',
      'line-height': '19px',
      "text-transform" : "uppercase",
      'text-decoration': 'none',
    }
  },
  ".pagination-link": {
    'default': {
      'margin': '0 5px',
      'padding': '5px',
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-weight': '700',
      'font-size': '18px',
      'line-height': '25px',
      'text-decoration': 'none',
    }
  },
  ".overline": {
    'default': {
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-weight': '700',
      'font-size': '14px',
      'line-height': '19px',
      "text-transform" : "uppercase",
      'text-decoration': 'none',
    }
  },
  "input": {
    'default': {
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-weight': '400',
      'font-size': '16px',
      'line-height': '22px',
      'padding' : `${GAP}px`
    }
  },
  "textarea": {
    'default': {
      'font-family': `${MAIN_FONT_FAMILY}, ${FMODS_FONT_FAMILY}, ${FONT_FAMILY_STYLE}`,
      'font-weight': '400',
      'font-size': '16px',
      'line-height': '22px',
      'padding' : `${GAP}px`,
      'color' : `${ON_BACKGROUND}`,
      'background-color': 'transparent'
    }
  }
}