const responsive = require('./responsive.json');

module.exports = {
    links : {
        "Typography": "/design-system/typography/",
        "Color Palette": "/design-system/color-palette/",
        "UI kit": "/design-system/ui-kit/",
        "Markdown": "/design-system/basic-markdown-syntax/",
        "Assets": "/design-system/assets/"
    },
    breakpoint: responsive.MEDIA.phone
}