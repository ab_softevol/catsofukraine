const fs = require('fs');
const { SitemapStream } = require('sitemap');
const moment = require('moment');

const siteData = require('./_data/site');

const elements = require("./_elements/index.js");
const utils = elements.utils;
const responsiveJS = elements.responsive;
const picture = elements.picture;
const designSystem = elements.designSystem;

// Content

async function bigCard(src, alt, name, sex, age, breed, price, link) {
  let card = `<a href=${link} class="card-big dec-none">
  ${(responsiveJS.buffer.call(this))}
  ${(await picture.picture.call(this, src, alt))}
  ${(responsiveJS.end_buffer.call(this))}
  <div class="buffer card-big_breed">
    ${(responsiveJS.buffer.call(this))}
      <p class="overline">${breed}</p>
    ${(responsiveJS.end_buffer.call(this))}
  </div>
  <div class="card-big__info-block text-center buffer">
    ${(responsiveJS.buffer.call(this))}
      <h2 class="title bold pb-0">${name}, ${sex}, ${getMonthFromTime(age)}
      </h2>
      <p class="h3 red">£${price}</p>
    ${(responsiveJS.end_buffer.call(this))}
  </div>
</a>`
  return card;
}

async function card(href, src, alt, title, time, preread) {
  if(preread != undefined){
    preread = `<p>${preread}</p>`
  } else{
    preread = ""
  }
  let card = `${(responsiveJS.buffer.call(this, "", "card"))}
  <a href = ${href}>
  <div class = "buffer card_container">
    ${(await picture.picture.call(this, src, alt))}
    ${(responsiveJS.buffer.call(this, "", "card_time"))}
    <p>
    <time datetime=${time}>${filterDate(time)}</time>
    </p>
    ${(responsiveJS.end_buffer.call(this))}
    <h2 class="h3">${title}</h2>
  </div>
</a>
${(responsiveJS.end_buffer.call(this))}`;

  utils.popFromWidthStack(this.page);
  return card;
}

async function fact(text, subscription=undefined) {
  let textContent = `<p class=" title bold">${text}</p>`;
  subscription != undefined ? textContent += `<p class=" title"><cite>${subscription}</cite></p>` : textContent
  let card = `<blockquote class="fact">
  <div class="spacer spacer-20"></div>
<div class="fact__container mx-auto">
<div class="fact__icon">
${(await picture.pictureSvgPng.call(this, "img/icons/paw.svg", "", "80", "80"))}
</div>
<div class="fact__inner">
${textContent}
</div>
</div>
<div class="spacer spacer-20"></div>
</blockquote>`;

  return card;
}

async function blockquote(text, subscription=undefined) {
  let textContent = `<p class=" title bold">${text}</p>`;
  subscription != undefined ? textContent += `<p class=" title"><cite>${subscription}</cite></p>` : textContent
  let card = `<blockquote class="fact">
  <div class="spacer spacer-20"></div>
<div class="fact__container mx-auto">
<div class="fact__icon">
${(await picture.pictureSvgPng.call(this, "img/icons/paw.svg", "", "80", "80"))}
</div>
<div class="fact__inner">
${textContent}
</div>
</div>
<div class="spacer spacer-20"></div>
</blockquote>`;

  return card;
}

async function gallery(imageList) {
  // console.log(`this is - `, this);
  let card = `<div class="gallery">`
  
  for (const src of imageList) {
    card += `${(responsiveJS.container.call(this, "100_50_50_50_50", "", "article-gallery_image"))}
    ${(responsiveJS.buffer.call(this))}
    ${(await picture.picture.call(this, src, "alt"))}
    ${(responsiveJS.end_buffer.call(this))}
    ${(responsiveJS.end_container.call(this))}`
  };

  card += `</div>`

  // console.log(`card is - `, card);
  return card;
}

function filterDate(dateLine) {
  const dateObject = moment(dateLine);
  return `${dateObject.format('MMMM')} ${dateObject.format('DD')}, ${dateObject.format('YYYY')}`;
}

function getMonthFromTime(dateLine) {
  dateLine = dateLine.split("/")
  let tmp = dateLine[1]
  dateLine[1] = dateLine[0]
  dateLine[0] = tmp
  dateLine = dateLine.join("/")
  const currdate = moment();
  const birthDate = moment(dateLine);
  let diff = currdate.diff(birthDate, "month");
  
  return (diff + " months") 
}

function getLast(array, value){
  let newObjArr = Object.values(array).slice(0,value)
  return newObjArr;
}

function extrudeCurrentPost(array, currentPostName){
  let newArr = array.filter(el=>el.data.title != currentPostName)
  return newArr;
}

function article_hero(content) {
  return `<div class="article-hero text-center"><section>${responsiveJS.max_width.call(this)}${content}${responsiveJS.end_max_width.call(this)}</section></div>`;
} 

module.exports = function (eleventyConfig) {
  eleventyConfig.ignores.add("README.md");
  eleventyConfig.addPassthroughCopy("assets/**/*");
  eleventyConfig.addPassthroughCopy("css/**/*.css");
  eleventyConfig.addPassthroughCopy("img/**/*");
  eleventyConfig.addPassthroughCopy("js/**/*");
  
  eleventyConfig.addFilter("filterCrubms", function(crumbLine) {
    crumbLine = crumbLine.slice(1).split("/")

    // Makes each word in array from capital letter
    crumbLine = crumbLine.map(el=>{
      return el.charAt(0).toUpperCase() + el.slice(1)
    })

    // Makes breadcrumb line from array
    crumbLine = crumbLine.join(" / ")

    return crumbLine
  });

  eleventyConfig.addFilter("getPageAmount", function(elemsCount) {
    res = Math.ceil(elemsCount / siteData.pagination);
    return res;
  });

  eleventyConfig.addFilter("append", function(str, str2) {
    return str + str2;
  });

  eleventyConfig.addFilter("isUrlSimilar", function(url, url2, chainAmount) {
    // Chain amount is the deph of url chain similarity checking
    // 1. /scottish/
    // 2. /scottish/2
    // 2 == 1 ! 1 != 2
    // console.log(`url - `,url);
    // console.log(`url 2 - `, url2);
    url = url.split("/").slice(1, -1)
    url2 = url2.split("/").slice(1, -1)   

    let similar = false
    if(!(url.length > url2.length)){
      loop1:
      for (let index = 0; index < url.length; index++) {
        // console.log(`1 == 2? ` ,url[index] === url2[index]);
        // console.log(`index is - `, index, ` chain amount is - `, chainAmount);
        if((url[index] === url2[index]) && ((index+1) == chainAmount)){
          // console.log("similarity");
          similar = true;
          break loop1;
        }else if((url[index] !== url2[index])){
          break loop1;
        }
      };
    }
    // console.log(`similar is - `, similar);
    return similar;
  });

  eleventyConfig.addFilter("filterDate", filterDate);

  eleventyConfig.addFilter("filterCrubmsShort", function(crumbLine) {
    crumbLine = crumbLine.slice(1).split("/")

    // Makes each word in array from capital letter
    crumbLine = crumbLine.map(el=>{
      return el.charAt(0).toUpperCase() + el.slice(1)
    })

    crumbLine.pop()
    crumbLine.pop()

    // Makes breadcrumb line from array
    crumbLine = crumbLine.join(" / ")

    return crumbLine
  });

  eleventyConfig.addFilter("filterTag", function(line) {
    if(line){
      line = line.split("-")

      // Makes each word in array from capital letter
      line = line.map(el=>{
        return el.charAt(0).toUpperCase() + el.slice(1)
      })
  
      // Makes breadcrumb line from array
      line = line.join(" ")
    }    

    return line
  });

  eleventyConfig.addFilter("filterKey", function(key) {
    key = key.split("-")

    // Makes each word in array from capital letter
    key = key.map(el=>{
      return el.charAt(0).toUpperCase() + el.slice(1)
    })

    // Makes breadcrumb line from array
    key = key.join(" ")

    return key
  });

  eleventyConfig.addFilter("extrudeCurrentPost", extrudeCurrentPost);
  eleventyConfig.addFilter("getLast", getLast);
  
  eleventyConfig.addCollection('tagList', require('./collections/breedList'));
  eleventyConfig.addCollection('pagedKittiesByTag', require('./collections/pagedKittiesByTag'));
  eleventyConfig.addCollection('cats', (coll)=>{
    return coll.getFilteredByGlob("./cats/*.md");
  });
  eleventyConfig.addCollection('news', (coll)=>{
    return coll.getFilteredByGlob("./news/*.md");
  });
  eleventyConfig.addCollection('articles', (coll)=>{
    return coll.getFilteredByGlob("./articles/*.md");
  });

  // Design System shortcodes

  eleventyConfig.addAsyncShortcode("assets_block", designSystem.assets_block);
  eleventyConfig.addAsyncShortcode("color_block", designSystem.color_block);
  eleventyConfig.addPairedAsyncShortcode("ui_kit_block", designSystem.ui_kit_block);


  // Responsive shortcodes
  eleventyConfig.addShortcode("max_width", responsiveJS.max_width);
  eleventyConfig.addShortcode("end_max_width", responsiveJS.end_max_width); 
  eleventyConfig.addPairedShortcode("responsive_container", responsiveJS.responsive_container);
  eleventyConfig.addShortcode("container", responsiveJS.container);
  eleventyConfig.addShortcode("end_container", responsiveJS.end_container);
  eleventyConfig.addShortcode("buffer", responsiveJS.buffer);
  eleventyConfig.addShortcode("end_buffer", responsiveJS.end_buffer);

  eleventyConfig.addAsyncShortcode("picture", picture.picture);
  eleventyConfig.addAsyncShortcode("pictureSvgPng", picture.pictureSvgPng);
  eleventyConfig.addAsyncShortcode("imagePngSelfSize", picture.imagePngSelfSize);

  eleventyConfig.addAsyncShortcode("bigCard", bigCard);
  eleventyConfig.addAsyncShortcode("card", card);
  eleventyConfig.addAsyncShortcode("fact", fact);
  eleventyConfig.addAsyncShortcode("blockquote", blockquote);
  eleventyConfig.addAsyncShortcode("gallery", gallery);  
  eleventyConfig.addPairedShortcode("article_hero", article_hero);  

  eleventyConfig.on('eleventy.after', async () => {
    fs.readdirSync('./_site/css/', { withFileTypes: true })
    .filter(d => d.isDirectory())
    .map(d => {
      if(d.name.slice(-4) === '.css') {
        fs.rmSync(`./_site/css/${d.name}`, { recursive: true, force: true });
      }
    });
    
    utils.getFiles('./_site/')
    .then((files) => {
      // Sitemap
      const conditions = ['\\sitemap.xml', '\\robots.txt', '\\js\\', '\\css\\', '\\img\\', '\\fonts\\'];
      const sitemap = new SitemapStream({ hostname: siteData.host });
      const writeStream = fs.createWriteStream('./_site/sitemap.xml');
      sitemap.pipe(writeStream);
      
      const indexPageId = files.findIndex(file => file.includes('_site\\index.html'))
      const indexPage = files.splice(indexPageId, 1);
      files.unshift(indexPage[0]);

      files.forEach(file => {
        if(!conditions.some(el => file.includes(el))) {
          file = (file.slice(file.indexOf('_site'))).replaceAll('\\', '/').replaceAll('_site/', siteData.host).replaceAll('index.html', '');
          sitemap.write({ url: file });
        }
      })
      sitemap.end();
      fs.writeFileSync('./_site/robots.txt', `User-agent: *
Allow: /

    Sitemap: ${siteData.host}sitemap.xml    
      `);
    })
    .catch(e => console.error(e));
  });
}