---
layout: base.njk
title:  Kittens For You
eleventyExcludeFromCollections: true
pagination:
    data: collections.pagedKittiesByTag
    size: 1
    alias: paged
permalink: "gallery/{{ paged.tagName }}/{% if paged.number > 1 %}{{ paged.number }}/{% endif %}index.html"
---

{% article_hero %}
    <div class="spacer spacer-40"></div>
    <h1>{{title}}</h1>
    <p class="breadcrumbs">{{page.url | filterCrubms | filterTag}}<span class="orange">Cats</span></p>
    <div class="spacer spacer-40"></div>
{% endarticle_hero %}

{% assign tagName = paged.tagName %}
{% include "partials/filter.njk" %}

<div class="gallery-main">
    <section>
        {% max_width %}
            {% responsive_container %}
                {%- for cat in paged.posts -%}
                {% container "100_50_33_33_33" %}
                    {% bigCard cat.data.heroPhoto, cat.data.catName, cat.data.catName, cat.data.catInfo.sex, cat.data.catInfo.date-of-birth, cat.data.catInfo.breed, cat.data.catPrice, cat.data.page.url %}
                {% end_container %}
                {%- endfor -%}
            {% endresponsive_container %}
        {% end_max_width %}
    </section>
</div>

{% assign pageAmount = collections.tagList[paged.tagName] | getPageAmount %}
{%- if pageAmount > 1 -%}
<div class="pagination text-center">
    <section>
    <div class="spacer spacer-20"></div>
    {% max_width %}
        {%- for pageNum in  (1..pageAmount) -%}
        {% if pageNum == 1 %}
        {%- assign url = "/gallery/" | append: paged.tagName | append: "/" -%}
        {%- endif -%}
        {%- if pageNum > 1 -%}
        {%- assign url = "/gallery/" | append: paged.tagName | append: "/" | append: pageNum | append: "/" -%} 
        {%- endif -%}
            <a href="{{url}}" aria-label="to page {{forloop.index}}" class="pagination-link{% if page.url == url %} current {% endif %}">{{forloop.index}}</a>
        {%- endfor -%}
    {% end_max_width %}
    </section>
</div>
{%- endif -%}

<div class="spacer spacer-90"></div>