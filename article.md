---
title:  Alex, male, 4 months
catName: Alex
catDescription: "The cat is similar in anatomy to the other felid species: it has a strong flexible body, quick reflexes, sharp teeth and retractable claws adapted to killing small prey. Its night vision and sense of smell are well developed."
description: The test page description
fact : "“It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.”"
catPrice: 620.00
birthDate: '2021-04-03'
catInfo: 
    sex : "Male"
    breed : "Scottish Fold"
    class : "Body"
    color : "Gray"
    feline-system : "Body"
    status : "Status"
catTerms: 
    vaccinated : "Full Vaccination Package"
    microchipped : "No"
    neuthering : "Upon Request"
    ready-to-go : "Immediately"
    current-location : "Toronto, Canada"
    payment-options : "Visa, Western Union, PayPal"
    delivery-options: "Worldwide"
galleryImages : 
    - "img/gallery1.png"
    - "img/gallery3.png"
    - "img/gallery2.png"
    - "img/gallery4.png"
    - "img/gallery5.png"
---

{% article_hero %}
    {% responsive_container %}
        <div class="spacer spacer-50"></div>
        <h1>{{title}}</h1>
        <p class="breadcrumbs">{{page.url | filterCrubms}}<span class="orange">{{catName}}</span></p>
        <div class="spacer spacer-50"></div>
    {% endresponsive_container %}
{% endarticle_hero %}

{% include 'partials/cat-info.njk' %}

{% blockquote fact %}

<div class="gallery">
    <section>

        {% responsive_container %}
        {% for imageUrl in galleryImages %}
            {% container "100_50_50_50_50", "", "article-gallery_image" %}
                {% buffer %}
                    {% picture  imageUrl, "" %}
                {% end_buffer %}
            {% end_container %}
        {% endfor %}
        {% endresponsive_container %}

    </section>
    <div class="spacer spacer-50"></div>
</div>

{% include 'partials/contact-section.njk' %}
<div class="spacer spacer-50"></div>
<div class="spacer spacer-50"></div>
<div class="spacer spacer-30"></div>
<div class="spacer spacer-90"></div>