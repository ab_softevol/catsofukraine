---
catName: Timon
title: Timon
tags: 
    - british
catDescription: "The cat is similar in anatomy to the other felid species: it has a strong flexible body, quick reflexes, sharp teeth and retractable claws adapted to killing small prey. Its night vision and sense of smell are well developed."
description: The test page description
catPrice: 900.00
heroPhoto: "img/kitten-5.png"
heroPhotoBig: "img/article-hero.png"
catInfo: 
    sex : "Male"
    date-of-birth : "12/07/2021"
    breed : "British"
    class : "Body"
    color : "White"
    feline-system : "Body"
    status : "Status"
catTerms: 
    vaccinated : "Full Vaccination Package"
    microchipped : "No"
    neuthering : "Upon Request"
    ready-to-go : "Immediately"
    current-location : "Turkey"
    payment-options : "Visa, Western Union, PayPal"
    delivery-options: "Worldwide"
galleryImages : 
    - "img/gallery1.png"
    - "img/gallery3.png"
    - "img/gallery4.png"
    - "img/gallery5.png"
    - "img/gallery2.png"
---