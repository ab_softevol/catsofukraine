---
layout: index.njk
title:  Main - the test page
description: The test page description
why-we:
    - {
        image: "img/icons/why-we_tg.svg",
        title: "Send us your request",
        description: "We get back in less than 24 hours"
    }
    - {
        image: "img/icons/why-we_super.svg",
        title: "We think of our reputation",
        description: "We demand responsibility from breeders"
    }
    - {
        image: "img/icons/why-we_group.svg",
        title: "Registered breeders only",
        description: "We check every breeder individually",
        last: true
    }

index-store:
    - {
        count: {
            countTitle: "142+",
            countSub: "Kittens For You",
            countDesc: "Raised With Love And Care",
            countDescSub: "Healthy and Tough, Friendly and Socialized" 
        }
    }
    - {
        image: "img/storefront-1.png",
        content: {
            isBest: "The best kittens",
            title: "Purebred With Responsibility",
            text: "WCF, TICA, CFA registered catteries",
            link: "/gallery/",
            btnText: "Visit Gallery"
        }
    }
    - {
        image: "img/storefront-banner.png"
    }
    - {
        image: "img/storefront-2.png",
        reversed: true,
        content: {
            isNew: true,
            title: "Pet Delivery Services",
            text: "Worldwide pet delivery by air or land transport",
            link: "/contacts/",
            btnText: "More Info"
        }
    }
    - {
        image: "img/storefront-3.png",
        content: {
            title: "Health Care Package For Each Kitten",
            text: "Microchipping, Vaccines, Deworming are a must. Any Tests and Screenings Upon Request",
            link: "/contacts/",
            btnText: "Shop Now"
        }
    }
    - {
        count: {
            countTitle: "120+",
            countSub: "Happy Families and counting ♡",
        }
    }

process:
    - {
        image: "img/how-it-works.svg",
        title: "Choose The Kitten You Love",
        description: "Each",
        i: "1"
    }
    - {
        image: "img/how-it-works.svg",
        title: "Request A Meeting",
        description: "Lorem ipsum dolor sit amet, consectetur acumsan lacus vel facilisis.",
        i: "2"
    }
    - {
        image: "img/how-it-works.svg",
        title: "Receive Custom Plan",
        description: "Lorem ipsum dolor sit amet, consectetur acumsan lacus vel facilisis.",
        i: "3"
    }

delivery:
    - {
        image: "img/delivery-1.svg",
        title: "Ask Any Questions Via Contact Form",
        description: "Got interested? Visit kitten’s page and fill in the Request Form!"
    }
    - {
        image: "img/delivery-2.svg",
        title: "Choose Appropriate Delivery Service",
        description: "Got interested? Visit kitten’s page and fill in the Request Form!"
    }
    - {
        image: "img/delivery-3.svg",
        title: "Ger Ready To Meet Your New Family Member",
        description: "Got interested? Visit kitten’s page and fill in the Request Form!"
    }

special:
    - {
        image: "img/special-left.png",
        title: "New: Siblings Adoption Service",
        description: "Look through the Individual Kitten Page and check if Siblings are also available",
        link: "/contacts/",
        linkName: "Find Out More"
    }
    - {
        image: "img/special-right.png",
        title: "Looking For Someone Very Special?",
        description: "You can try our Specific Search Service:find <span class='bold'>specific breed / color / sex / age</span> kitten!",
        link: "/contacts/",
        linkName: "Fill in the Form"
    }
---

<div class="main">
<main>

<div class="main-hero">
    <section>
        {% max_width %}
            {% responsive_container "", "hero_content-wrapper" %}
                {% container "100_100_50_50_50", "", "float-left img-w-100 hero-content_image img-block-inside" %}
                    {% picture "img/main-hero.png", "Our kittens" %}
                {% end_container %}
                {% container "100_100_50_50_50", "", "hero-content_text ml-auto my-auto float-left" %}
                    <p class="h3">wfwrfqfqrvqrf</p>
                    <h1 class="orange ">100% Purebred Kittens</h1>
                    <p class="h3">Healthy kittens from WCF / CFA / TICA catteries</p>
                    <div class="spacer spacer-20"></div>
                    {% responsive_container "", "main-hero_price" %}
                        {% responsive_container "", "ml-auto" %}
                            {% responsive_container "", "mt-auto" %}
                                <p class="float-left my-auto">Starting at</p>
                                <p class="h3 float-left my-auto">$450.00</p>
                            {% endresponsive_container %}
                        {% endresponsive_container %}
                        <div class="main-hero_image-wrapper">
                            {% buffer %}
                                <a href="/gallery/" class="button"><span>Visit Gallery</span></a>
                            {% end_buffer %}
                        </div>
                    {% endresponsive_container %}
                {% end_container %}
            {% endresponsive_container %}
        {% end_max_width %}
    </section>
</div>

<div class="why-we">
<section>
<div class="spacer spacer-20"></div>
        {% max_width %}
        {% responsive_container "", "why-we_container" %}

        {%- for item in why-we -%}
        <div class="float-left why-we_item-container">
                {% responsive_container "", "why-we_item nowrap" %}
                    <div class="float-left my-auto img-block-inside" role="presentation">
{% assign image = item.image %}
{% pictureSvgPng image, "", "55", "55" %}
                    </div>
                    <div class="float-left">
                        <h2 class="h3 pb-0">{{ item.title}}</h2>
                        <p>{{ item.description}}</p>
                    </div>
                {% endresponsive_container %}
            </div>
{% if item.last != true %}
<div class="why-we_separator float-left"></div>
{% endif %}
        {%- endfor -%}

        {% endresponsive_container %}
        {% end_max_width %}
<div class="spacer spacer-20"></div>
</section>
</div>

<section>
<div class="spacer-80"></div>
<div class="index-store">
{% max_width %}
{% responsive_container "", "masonry-grid" %}

  {% container "100_100_50_50_50", "", "masonry-grid__item masonry-grid__item--1" %}
    {% buffer %}
      {% responsive_container "", "index-store__card index-store__card--1" %}
        <div class="float-left">
          {% buffer "", "pr-0" %}
            <div class="border">
              <p class="h1 pa-0">{{index-store[0].count.countTitle}}</p>
              <p class="pa-0">{{index-store[0].count.countSub}}</p>
            </div>
          {% end_buffer %}
        </div>
        <div class="float-left my-auto">
          <p class="h3 pb-0">{{index-store[0].count.countDesc}}</p>
          <p>{{index-store[0].count.countDescSub}}</p>
        </div>
      {% endresponsive_container %}
    {% end_buffer %}
  {% end_container %}

  {% container "100_100_50_50_50", "", "masonry-grid__item masonry-grid__item--2" %}
    {% buffer %}
      <div class="index-store__card index-store__card--3">
{% assign image = index-store[1].image %}
{% picture image, "" %}
        <div class="index-store__textblock">
          <div class="index-store__text">
            <p class="brown bold">{{index-store[1].content.isBest}}</p>
            <p class="h2">{{index-store[1].title}}</p>
            <p>{{index-store[1].content.text}}</p>
          </div>
          {% responsive_container "", "index-store__link-wrapper" %}
            {% buffer "", "index-store__link-container" %}
              <a href="/gallery/" aria-label="{{index-store[1].content.btnText}} (link to {{index-store[1].text}})" class="button d-ib"><span>{{index-store[1].content.btnText}}</span></a>
            {% end_buffer %}
          {% endresponsive_container %}
        </div>
      </div>
    {% end_buffer %}
  {% end_container %}

  {% container "100_100_50_50_50", "", "masonry-grid__item masonry-grid__item--3" %}
    {% buffer %}
      <div class="index-store__card index-store__card--5">
{% assign image = index-store[2].image %}
{% picture image, "" %}
      </div>
    {% end_buffer %}
  {% end_container %}

  {% container "100_100_50_50_50", "", "masonry-grid__item masonry-grid__item--4" %}
    {% buffer %}
      <div class="index-store__card index-store__card--2">
{% assign image = index-store[3].image %}
{% picture image, "" %}
        <div class="index-store__textblock">
          <div class="index-store__text">
            {% buffer %}
              <p class="index-store__label">New</p>
            {% end_buffer %}
            <p class="h2">{{ index-store[3].content.title }}</p>
            <p>{{ index-store[3].content.text }}</p>
          </div>
          {% responsive_container "", "index-store__link-wrapper" %}
            {% buffer "", "index-store__link-container" %}
              <a href="/contacts/" aria-label="{{ index-store[3].content.btnText }} (link to {{ index-store[3].content.text }})" class="button d-ib dec-none"><span>{{ index-store[3].content.btnText }}</span></a>
            {% end_buffer %}
          {% endresponsive_container %}
        </div>
      </div>
    {% end_buffer %}
  {% end_container %}

  {% container "100_100_50_50_50", "", "masonry-grid__item masonry-grid__item--5" %}
    {% buffer %}
      <div class="index-store__card index-store__card--4">
{% assign image = index-store[4].image %}
{% picture image, "" %}
        <div class="index-store__textblock">
          <div class="index-store__text">
            <p class="h2">{{ index-store[4].content.title }}</p>
            <p>{{ index-store[4].content.text }}</p>
          </div>
          {% responsive_container "", "index-store__link-wrapper" %}
            {% buffer "", "index-store__link-container" %}
              <a href="/contacts/" aria-label="{{ index-store[4].content.btnText }} (link to {{ index-store[4].content.text }})" class="button d-ib dec-none"><span>{{ index-store[4].content.btnText }}</span></a>
            {% end_buffer %}
          {% endresponsive_container %}
        </div>
      </div>
    {% end_buffer %}
  {% end_container %}

  {% container "100_100_50_50_50", "", "masonry-grid__item masonry-grid__item--6" %}
    {% buffer %}
    <div class="text-center">
      <div class="index-store__card index-store__card--6">
        <p class="h1">{{ index-store[5].count.countTitle }}</p>
        <p>{{ index-store[5].count.countSub }}</p>
      </div>
    </div>
    {% end_buffer %}
  {% end_container %}

{% endresponsive_container %}
{% end_max_width %}
<div class="spacer-80"></div>
</div>
</section>

<section class="how-it-works">
    <div class="how-it-works_wrapper">
        <div class="spacer spacer-90"></div>
        {% max_width %}
            <div class="text-center">
                <h2 class="p">How It Works</h2>
                <p class="h2">Check Out Our Work Process!</p>
            </div>
            <div class="spacer spacer-50"></div>
            {% responsive_container "", "how-it-works_container" %}
                {%- for item in process -%}
                {% container "100_100_33_33_33", "", "text-center"%}
                    {% buffer %}
                        {% container "54_54_54_54_54", "", "how-it-works_image mx-auto img-w-100 img-block-inside" %}
{% assign image = item.image %}
{% pictureSvgPng image, "", "190", "190" %}
                            <p class="how-it-works_image-num">0{{item.i}}</p>
                        {% end_container %}
                    {% end_buffer %}
                    <h3>{{item.title}}</h3>
                    <p>{{item.description}}</p>
                {% end_container %}
                {%- endfor -%}
            {% endresponsive_container %}
        {% end_max_width %}
        <div class="spacer spacer-90"></div>
    </div>
</section>

<section class="special">
<div class="spacer spacer-80"></div>

{% max_width "wide" %}
    {% responsive_container "", "special_container" %}
    {%- for item in special -%}
    {% container "100_100_50_50_50" %}
        {% buffer "", "special_continer-side" %}
            <div class="special_continer-side_wrapper">
{% assign image = item.image %}
{% picture image, "" %}
                <div class="special_continer-side_content special">
                    <h2 class="h3">{{item.title}}</h2>
                    <p class="continer-side_content_subtitile">{{item.description}}</p>
                    <div class="buffer mx-auto d-ib">
                        <a href={{item.link}} class="button dec-none"><span>{{item.linkName}}</span></a>
                    </div>
                </div>
            </div>
        {% end_buffer %}
    {% end_container %}
    {%- endfor -%}
    {% endresponsive_container %}
{% end_max_width %}

<div class="spacer spacer-60"></div>
</section>

<section class="delivery">
    <div class="delivery_wrapper">
        {% max_width %}
            {% responsive_container "", "delivery_container" %}
                {%- for item in delivery -%}
                {% container "100_100_33_33_33", "", "text-center"%}
                    {% buffer %}
                        <div class="delivery-block">
                            <div class="spacer spacer-20"></div>
                            <div class="delivery_image mx-auto">
{% assign image = item.image %}
{% pictureSvgPng image, "", "60", "60" %}
                            </div>
                            <p class="delivery_image-num">01</p>
                            <h3>{{item.title}}</h3>
                            <p class="got gray-500-color">{{item.description}}</p>
                            <div class="spacer spacer-20"></div>
                        </div>
                    {% end_buffer %}
                {% end_container %}
                {%- endfor -%}
            {% endresponsive_container %}
        {% end_max_width %}
    </div>
    <div class="spacer spacer-70"></div>
</section>

<section class="recently">
    {% max_width %}
    <div class="recently_header text-center">
        <h2 class="lined-header d-ib">
            Recently added kittens
        </h2>
    </div>
    <div class="spacer spacer-40"></div>
    <div class="recently_container">
        <div class="glide">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                {%- assign recentlyPosts = collections.cats | getLast: 6 %}
                {%- for cat in recentlyPosts -%}
                <li class="glide__slide">
                {%- container "100_50_33_33_33", "", "text-center"-%}
{%- assign alt = post.data.title | append : "`s photo" -%}
{%- bigCard cat.data.heroPhoto, alt, cat.data.title, cat.data.catInfo.sex, cat.data.catInfo.date-of-birth, cat.data.catInfo.breed, cat.data.catPrice, cat.url -%}
                {%- end_container -%}
                </li>
                {%- endfor -%}
                </ul>
            </div>
        </div>
    </div>
    <div class="spacer spacer-40"></div>
    {% end_max_width %}
</section>

<section class="also">
    {% max_width %}
        <div class="spacer spacer-30"></div>
        {% responsive_container %}
            {% container "100_100_50_50_50" %}
            <div class="spacer spacer-100"></div>
                <div class="also__card-left">
                    {% container "100_65_65_65_65", "", "mx-auto" %}
                        {% buffer %}
                            {% picture "img/also_left.png", title %}
                        {% end_buffer %}
                    {% end_container %}
                    <div class="aslo__card-text buffer">
                        <h2>Exclusive Offers</h2>
                        <p class="h1 orange">40% OFF</p>
                        {% buffer %}
                            <a href="/contacts/" class="button dec-none d-ib"><span>Shop Now</span></a>
                        {% end_buffer %}
                    </div>
                </div>
            {% end_container %}
            {% container "100_100_50_50_50" %}
                {% buffer %}
                    <div class="also__card-right">
                {% picture "img/also-right.png", title %}
                        <div class="spacer spacer-75"></div>
                        <div class="aslo__card-text buffer">
                            {% buffer %}
                                <p class="gray-label">Info for Breeders</p>
                            {% end_buffer %}
                            <h2>How To Add Your Kitten</h2>
                            <p class="gray-500-color">Fill in the form, and we’ll get back shortly.</p>
                            {% buffer %}
                                <a href="/contacts/" class="button dec-none d-ib"><span>Read Our Rules</span></a>
                            {% end_buffer %}
                        </div>
                    </div>
                {% end_buffer %}
            {% end_container %}
        {% endresponsive_container %}
    {% end_max_width %}
    <div class="spacer spacer-80"></div>
</section>

</main>
</div>
