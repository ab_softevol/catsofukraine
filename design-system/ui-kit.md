---
layout: design-system/ui-kit.njk
title: UI-Kit
---

## Links

### Link

{% responsive_container %}
{% ui_kit_block "", "light" %}
  <a href="#">Rest</a>
{% endui_kit_block %}

{% ui_kit_block 'hover', "light" %}
  <a href="#">Hover</a>
{% endui_kit_block %}

{% ui_kit_block 'active', "light" %}
  <a href="#">Active</a>
{% endui_kit_block %}

{% ui_kit_block 'focus', "light" %}
  <a href="#">Focus</a>
{% endui_kit_block %}
{% endresponsive_container %}

### Header Link

{% responsive_container %}
{% ui_kit_block "", "" %}
  <a class="main-menu__link" href="/" role="menuitem" tabindex="0">Typography</a>
{% endui_kit_block %}

{% ui_kit_block 'hover', "" %}
  <a class="main-menu__link" href="/" role="menuitem" tabindex="0">Typography</a>
{% endui_kit_block %}

{% ui_kit_block 'active', "" %}
  <a class="main-menu__link" href="/" role="menuitem" tabindex="0">Typography</a>
{% endui_kit_block %}

{% ui_kit_block 'focus', "" %}
  <a class="main-menu__link" href="/" role="menuitem" tabindex="0">Typography</a>
{% endui_kit_block %}

{% ui_kit_block 'current', "" %}
  <a class="main-menu__link" href="/" role="menuitem" tabindex="0">Typography</a>
{% endui_kit_block %}
{% endresponsive_container %}

### Social links

{% responsive_container %}
{% ui_kit_block "", "" %}
  <a class="social-links__link" target="_blank" rel="noopener"
  href="#" aria-label="instagram">
    {% pictureSvgPng "img/icons/inst.svg", "instagram", "32", "32" %}
  </a>
{% endui_kit_block %}

{% ui_kit_block 'hover', "" %}
<a class="social-links__link" target="_blank" rel="noopener"
href="#" aria-label="instagram">
  {% pictureSvgPng "img/icons/inst.svg", "instagram", "32", "32" %}
</a>
{% endui_kit_block %}

{% ui_kit_block "active", "" %}
  <a class="social-links__link" target="_blank" rel="noopener"
    href="#" aria-label="instagram">
    {% pictureSvgPng "img/icons/inst.svg", "instagram", "32", "32" %}
  </a>
{% endui_kit_block %}

{% ui_kit_block 'focus', "" %}
<a class="social-links__link" target="_blank" rel="noopener"
  href="#" aria-label="instagram">
  {% pictureSvgPng "img/icons/inst.svg", "instagram", "32", "32" %}
</a>
{% endui_kit_block %}
{% endresponsive_container %}

{% responsive_container %}
{% ui_kit_block "", "" %}
  <a class="social-links__link" target="_blank" rel="noopener"
  href="#" aria-label="facebook">
    {% pictureSvgPng "img/icons/fb.svg", "facebook", "32", "32" %}
  </a>
{% endui_kit_block %}

{% ui_kit_block 'hover', "" %}
<a class="social-links__link" target="_blank" rel="noopener"
href="#" aria-label="facebook">
  {% pictureSvgPng "img/icons/fb.svg", "facebook", "32", "32" %}
</a>
{% endui_kit_block %}

{% ui_kit_block "active", "" %}
  <a class="social-links__link" target="_blank" rel="noopener"
    href="#" aria-label="facebook">
    {% pictureSvgPng "img/icons/fb.svg", "facebook", "32", "32" %}
  </a>
{% endui_kit_block %}

{% ui_kit_block 'focus', "" %}
<a class="social-links__link" target="_blank" rel="noopener"
  href="#" aria-label="facebook">
  {% pictureSvgPng "img/icons/fb.svg", "facebook", "32", "32" %}
</a>
{% endui_kit_block %}
{% endresponsive_container %}

### Pagination link

{% responsive_container %}
{% ui_kit_block "", "" %}
  <a href="/" aria-label="to page" class="pagination-link">
    1
  </a>
{% endui_kit_block %}

{% ui_kit_block 'hover', "" %}
  <a href="/" aria-label="to page" class="pagination-link">
    1
  </a>
{% endui_kit_block %}

{% ui_kit_block "active", "" %}
  <a href="/" aria-label="to page" class="pagination-link">
    1
  </a>
{% endui_kit_block %}

{% ui_kit_block 'focus', "" %}
  <a href="/" aria-label="to page" class="pagination-link">
    1
  </a>
{% endui_kit_block %}

{% ui_kit_block 'current', "" %}
  <a href="/" aria-label="to page" class="pagination-link">
    1
  </a>
{% endui_kit_block %}
{% endresponsive_container %}

### Filter link

{% responsive_container %}
{% ui_kit_block "", "" %}
<a class="filter-link" href="/">Filter</a>
{% endui_kit_block %}

{% ui_kit_block 'hover', "" %}
<a class="filter-link" href="/">Filter</a>
{% endui_kit_block %}

{% ui_kit_block "active", "" %}
<a class="filter-link" href="/">Filter</a>
{% endui_kit_block %}

{% ui_kit_block 'focus', "" %}
<a class="filter-link" href="/">Filter</a>
{% endui_kit_block %}

{% ui_kit_block 'current', "" %}
<a class="filter-link" href="/">Filter</a>
{% endui_kit_block %}
{% endresponsive_container %}

## Buttons

### button

{% responsive_container %}
{% ui_kit_block "", "light" %}
  <button class="button" type="button"><span>Rest</span></button>
{% endui_kit_block %}

{% ui_kit_block 'hover', "light" %}
  <button class="button" type="button"><span>Hover</span></button>
{% endui_kit_block %}

{% ui_kit_block 'active', "light" %}
  <button class="button" type="button"><span>Active</span></button>
{% endui_kit_block %}

{% ui_kit_block 'focus', "light" %}
  <button class="button" type="button"><span>Focus</span></button>
{% endui_kit_block %}
{% endresponsive_container %}

### button-colored

{% responsive_container %}
{% ui_kit_block "", "light" %}
  <button class="button-colored" type="button"><span>Rest</span></button>
{% endui_kit_block %}

{% ui_kit_block 'hover', "light" %}
  <button class="button-colored" type="button"><span>Hover</span></button>
{% endui_kit_block %}

{% ui_kit_block 'active', "light" %}
  <button class="button-colored" type="button"><span>Active</span></button>
{% endui_kit_block %}

{% ui_kit_block 'focus', "light" %}
  <button class="button-colored" type="button"><span>Focus</span></button>
{% endui_kit_block %}
{% endresponsive_container %}

### burger__button

{% responsive_container %}
{% ui_kit_block "", "" %}
<button class="burger__button" id="burger-button" aria-haspopup="true" aria-expanded="false" aria-label="Open menu" style="display: block; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTAgMTVIMThWMTNIMFYxNVpNMCAxMEgxOFY4SDBWMTBaTTAgM1Y1SDE4VjNIMFoiIGZpbGw9IiMyRDJEMkQiLz4KPC9zdmc+Cg==&quot;);"></button>
{% endui_kit_block %}

{% ui_kit_block "hover", "" %}
<button class="burger__button" id="burger-button" aria-haspopup="true" aria-expanded="false" aria-label="Open menu" style="display: block; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTAgMTVIMThWMTNIMFYxNVpNMCAxMEgxOFY4SDBWMTBaTTAgM1Y1SDE4VjNIMFoiIGZpbGw9IiMyRDJEMkQiLz4KPC9zdmc+Cg==&quot;);"></button>
{% endui_kit_block %}

{% ui_kit_block "active", "" %}
<button class="burger__button" id="burger-button" aria-haspopup="true" aria-expanded="false" aria-label="Open menu" style="display: block; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTAgMTVIMThWMTNIMFYxNVpNMCAxMEgxOFY4SDBWMTBaTTAgM1Y1SDE4VjNIMFoiIGZpbGw9IiMyRDJEMkQiLz4KPC9zdmc+Cg==&quot;);"></button>
{% endui_kit_block %}

{% ui_kit_block "focus", "" %}
<button class="burger__button" id="burger-button" aria-haspopup="true" aria-expanded="false" aria-label="Open menu" style="display: block; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTAgMTVIMThWMTNIMFYxNVpNMCAxMEgxOFY4SDBWMTBaTTAgM1Y1SDE4VjNIMFoiIGZpbGw9IiMyRDJEMkQiLz4KPC9zdmc+Cg==&quot;);"></button>
{% endui_kit_block %}

{% endresponsive_container %}

### burger__button--opened

{% responsive_container %}
{% ui_kit_block "", "" %}
<button class="burger__button" id="burger-button" aria-haspopup="true" aria-expanded="true" aria-label="Close menu" style="display: block; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTE2IDMuNDFMMTQuNTkgMkw5IDcuNTlMMy40MSAyTDIgMy40MUw3LjU5IDlMMiAxNC41OUwzLjQxIDE2TDkgMTAuNDFMMTQuNTkgMTZMMTYgMTQuNTlMMTAuNDEgOUwxNiAzLjQxWiIgZmlsbD0iIzJEMkQyRCIvPgo8L3N2Zz4K&quot;);"></button>
{% endui_kit_block %}

{% ui_kit_block "hover", "" %}
<button class="burger__button" id="burger-button" aria-haspopup="true" aria-expanded="true" aria-label="Close menu" style="display: block; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTE2IDMuNDFMMTQuNTkgMkw5IDcuNTlMMy40MSAyTDIgMy40MUw3LjU5IDlMMiAxNC41OUwzLjQxIDE2TDkgMTAuNDFMMTQuNTkgMTZMMTYgMTQuNTlMMTAuNDEgOUwxNiAzLjQxWiIgZmlsbD0iIzJEMkQyRCIvPgo8L3N2Zz4K&quot;);"></button>
{% endui_kit_block %}

{% ui_kit_block "active", "" %}
<button class="burger__button" id="burger-button" aria-haspopup="true" aria-expanded="true" aria-label="Close menu" style="display: block; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTE2IDMuNDFMMTQuNTkgMkw5IDcuNTlMMy40MSAyTDIgMy40MUw3LjU5IDlMMiAxNC41OUwzLjQxIDE2TDkgMTAuNDFMMTQuNTkgMTZMMTYgMTQuNTlMMTAuNDEgOUwxNiAzLjQxWiIgZmlsbD0iIzJEMkQyRCIvPgo8L3N2Zz4K&quot;);"></button>
{% endui_kit_block %}

{% ui_kit_block "focus", "" %}
<button class="burger__button" id="burger-button" aria-haspopup="true" aria-expanded="true" aria-label="Close menu" style="display: block; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTE2IDMuNDFMMTQuNTkgMkw5IDcuNTlMMy40MSAyTDIgMy40MUw3LjU5IDlMMiAxNC41OUwzLjQxIDE2TDkgMTAuNDFMMTQuNTkgMTZMMTYgMTQuNTlMMTAuNDEgOUwxNiAzLjQxWiIgZmlsbD0iIzJEMkQyRCIvPgo8L3N2Zz4K&quot;);"></button>
{% endui_kit_block %}

{% endresponsive_container %}

### slider-button prev

{% responsive_container %}
{% ui_kit_block "", "light" %}
  <button type="button" class="glide__arrow glide__arrow--prev" data-glide-dir="<" aria-label="previous slide"></button>
{% endui_kit_block %}

{% ui_kit_block 'hover', "light" %}
  <button type="button" class="glide__arrow glide__arrow--prev" data-glide-dir="<" aria-label="previous slide"></button>
{% endui_kit_block %}

{% ui_kit_block 'active', "light" %}
  <button type="button" class="glide__arrow glide__arrow--prev" data-glide-dir="<" aria-label="previous slide"></button>
{% endui_kit_block %}

{% ui_kit_block 'focus', "light" %}
  <button type="button" class="glide__arrow glide__arrow--prev" data-glide-dir="<" aria-label="previous slide"></button>
{% endui_kit_block %}
{% endresponsive_container %}

### slider-button next

{% responsive_container %}
{% ui_kit_block "", "light" %}
  <button type="button" class="glide__arrow glide__arrow--next" data-glide-dir=">" aria-label="next slide"></button>
{% endui_kit_block %}

{% ui_kit_block 'hover', "light" %}
  <button type="button" class="glide__arrow glide__arrow--next" data-glide-dir=">" aria-label="next slide"></button>
{% endui_kit_block %}

{% ui_kit_block 'active', "light" %}
  <button type="button" class="glide__arrow glide__arrow--next" data-glide-dir=">" aria-label="next slide"></button>
{% endui_kit_block %}

{% ui_kit_block 'focus', "light" %}
  <button type="button" class="glide__arrow glide__arrow--next" data-glide-dir=">" aria-label="next slide"></button>
{% endui_kit_block %}
{% endresponsive_container %}

## Fields

### Input

{% responsive_container %}
{% ui_kit_block "", "light" %}
<label class="visually-hidden" for="field-1">
  Label
</label>
<div class="text-field">
  <input class="text-field__input" type="text" name="field-1" id="field-1" placeholder="Rest" required="">
</div>
{% endui_kit_block %}

{% ui_kit_block 'focus', "light" %}
<label class="visually-hidden" for="field-2">
  Label
</label>
<div class="text-field">
  <input class="text-field__input" type="text" name="field-2" id="field-2" placeholder="Focus" required="">
</div>
{% endui_kit_block %}

{% ui_kit_block 'disabled', "light" %}
<label class="visually-hidden" for="field-3">
  Label
</label>
<div class="text-field">
  <input class="text-field__input" type="text" name="field-3" id="field-3" placeholder="Disabled" required="">
</div>
{% endui_kit_block %}

{% ui_kit_block 'error', "light" %}
<label class="visually-hidden" for="field-4">
  Label
</label>
<div class="text-field">
  <input class="text-field__input" type="text" name="field-4" id="field-4" placeholder="Error" required="">
</div>
{% endui_kit_block %}
{% endresponsive_container %}

### Textarea

{% responsive_container %}
{% ui_kit_block "", "light" %}
<label class="visually-hidden" for="field-5">
  Label
</label>
<div class="text-field">
  <textarea class="text-field__input" type="text" name="field-5" id="field-5" placeholder="Rest" required=""></textarea>
</div>
{% endui_kit_block %}

{% ui_kit_block 'focus', "light" %}
<label class="visually-hidden" for="field-6">
  Label
</label>
<div class="text-field">
  <textarea class="text-field__input" type="text" name="field-6" id="field-6" placeholder="Focus" required=""></textarea>
</div>
{% endui_kit_block %}

{% ui_kit_block 'disabled', "light" %}
<label class="visually-hidden" for="field-7">
  Label
</label>
<div class="text-field">
  <textarea class="text-field__input" type="text" name="field-7" id="field-7" placeholder="Disabled" required=""></textarea>
</div>
{% endui_kit_block %}

{% ui_kit_block 'error', "light" %}
<label class="visually-hidden" for="field-8">
  Label
</label>
<div class="text-field">
  <textarea class="text-field__input" type="text" name="field-8" id="field-8" placeholder="Error" required=""></textarea>
</div>
{% endui_kit_block %}
{% endresponsive_container %}