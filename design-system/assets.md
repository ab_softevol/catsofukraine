---
layout: design-system/assets.njk
title: Assets
---

{% max_width %}

# {{ title }}

## Logo

{% responsive_container %}
{% assets_block "/img/logo.png", "150", "46", "" %}
{% endresponsive_container %}

## Icons

{% responsive_container %}
{% assets_block "/img/icons/fb.svg", "32", "32", "" %}
{% assets_block "/img/icons/inst.svg", "32", "32", "" %}
{% assets_block "/img/icons/blockquote.svg", "80", "80", "" %}
{% assets_block "/img/icons/paw.svg", "80", "80", "" %}
{% assets_block "/img/icons/post-fb.svg", "30", "30", "" %}
{% assets_block "/img/icons/post-inst.svg", "30", "30", "" %}
{% assets_block "/img/icons/post-tw.svg", "30", "30", "" %}
{% assets_block "/img/icons/telegram.svg", "24", "24", "" %}
{% assets_block "/img/icons/whatsapp.svg", "24", "24", "" %}
{% assets_block "/img/icons/why-we_group.svg", "55", "55", "" %}
{% assets_block "/img/icons/why-we_super.svg", "55", "55", "" %}
{% assets_block "/img/icons/why-we_tg.svg", "55", "55", "" %}
{% endresponsive_container %}

## Buttons

{% responsive_container %}
{% assets_block "/img/buttons/article-arrow-left.svg", "20", "8", "" %}
{% assets_block "/img/buttons/article-arrow-right.svg", "20", "8", "" %}
{% assets_block "/img/buttons/button-arrow-black.png", "8", "9", "", "light" %}
{% assets_block "/img/buttons/button-arrow-gray.png", "8", "9", "" %}
{% assets_block "/img/buttons/button-arrow-white.png", "8", "9", "" %}
{% endresponsive_container %}

{% end_max_width %}
