# tabbyfamily.com

## resources

### design

https://www.figma.com/file/3YlhZjLzjdSPf2dneyvfpU/catsofukraine?t=CskBZfI9P1Q0s8mq-6

### gitlab

https://gitlab.com/olgailyinova/tabbyfamily.com

### dev version

### release build

http://www.tabbyfamily.com

## commands

### install dependencies

`npm install`

### run eleventy

`eleventy --serve`

### build site

`eleventy`
