---
layout: base.njk
title:  Articles
pagination:
  data: collections.articles
  size: 6
permalink: "articles/{% if pagination.pageNumber > 0 %}{{ pagination.pageNumber | plus: 1  }}/{% endif %}index.html"
---

{% article_hero %}
<div class="spacer spacer-40"></div>

# {{title}}

<div class="spacer spacer-40"></div>
{% endarticle_hero %}

<div class="gallery-main">
    <div class="spacer spacer-30"></div>
    <section>
        {% max_width %}
            {% responsive_container %}
                {%- for post in pagination.items -%}
                {% container "100_50_33_33_33" %}
                    {% card post.url, post.data.heroImage, post.data.title, post.data.title, post.data.date %}
                {% end_container %}
                {%- endfor -%}
            {% endresponsive_container %}
        {% end_max_width %}
    </section>
</div>

<div class="pagination text-center">
    <section>
        <div class="spacer spacer-30"></div>
        {% max_width %}
            {%- for item in pagination.hrefs -%}
                <a href={{item}} aria-label="to page {{forloop.index}}" class="pagination-link{% if page.url == item %} current {% endif %}">
                {{ forloop.index }}
                </a>
            {%- endfor -%}
        {% end_max_width %}
        <div class="spacer spacer-100"></div>
    </section>
</div>