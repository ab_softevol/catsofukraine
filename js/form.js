(function() {
  if(document.createElement('form').noValidate !== undefined) {
    var form = document.querySelector('.contact-form form');
    form.noValidate = true;
    var nameBlock = form.querySelector('.text-field--name');
    var LastNameBlock = form.querySelector('.text-field--last-name');
    var emailBlock = form.querySelector('.text-field--email');
    var textBlock = form.querySelector('.text-field--message');
    var subjectBlock = form.querySelector('.text-field--subject');
    var name = form.querySelector('#user-name');
    var last_name = form.querySelector('#user-last-name');
    var email = form.querySelector('#user-email');
    var text = form.querySelector('#user-message');
    var subject = form.querySelector('#user-subject');
    var submit = form.querySelector('button');

    var FieldFlag= {
      "name": false,
      "email": false,
      "comment": false,
      "last_name": false,
      "subjectBlock": false
    };
  
    var FieldFirstBlur = {
      "name": false,
      "email": false,
      "comment": false,
      "last_name": false,
      "subjectBlock": false
    };


    function checkAreas(){
      if(
        (FieldFlag.email && FieldFlag.comment) ||
        (FieldFlag.email && !FieldFirstBlur.comment) || 
        (!FieldFirstBlur.email && FieldFlag.comment)) {
        submit.removeAttribute('disabled');
      }
    }
  
    function nameCheck() {
      FieldFirstBlur.name = true;
      var errorMessage = nameBlock.querySelector(".text-field__message");
  
      if(errorMessage) {
        nameBlock.removeChild(errorMessage);
        errorMessage = null;
      }
  
      if(name.value === '') {
        errorMessage = document.createElement('span');
        errorMessage.classList.add('text-field__message');
        errorMessage.textContent = 'Field should be not empty';
        nameBlock.appendChild(errorMessage);
        nameBlock.classList.add('error');
        submit.setAttribute('disabled', 'disabled');
        FieldFlag.name = false;
      } else {
        nameBlock.classList.remove('error');
        FieldFlag.name = true;

        checkAreas()
      }
    }

    function subjectCheck() {
      FieldFirstBlur.name = true;
      var errorMessage = subjectBlock.querySelector(".text-field__message");

      if(errorMessage) {
        subjectBlock.removeChild(errorMessage);
        errorMessage = null;
      }
  
      if(name.value === '') {
        errorMessage = document.createElement('span');
        errorMessage.classList.add('text-field__message');
        errorMessage.textContent = 'Field should be not empty';
        subjectBlock.appendChild(errorMessage);
        subjectBlock.classList.add('error');
        submit.setAttribute('disabled', 'disabled');
        FieldFlag.subjectBlock = false;
      } else {
        subjectBlock.classList.remove('error');
        FieldFlag.subjectBlock = true;

        checkAreas()
      }
    }

    function lastNameCheck() {
      FieldFirstBlur.name = true;
      var errorMessage = LastNameBlock.querySelector(".text-field__message");
  
      if(errorMessage) {
        LastNameBlock.removeChild(errorMessage);
        errorMessage = null;
      }
  
      if(last_name.value === '') {
        errorMessage = document.createElement('span');
        errorMessage.classList.add('text-field__message');
        errorMessage.textContent = 'Field should be not empty';
        LastNameBlock.appendChild(errorMessage);
        LastNameBlock.classList.add('error');
        submit.setAttribute('disabled', 'disabled');
        FieldFlag.last_name = false;
      } else {
        LastNameBlock.classList.remove('error');
        FieldFlag.last_name = true;

        checkAreas()
      }
    }

    function validateEmail(email) {
      var re = /\S+@\S+\.\S+/;
      return re.test(email);
    }
  
    function emailCheck() {
      FieldFirstBlur.email = true;
      var errorMessage = emailBlock.querySelector(".text-field__message");

      if(errorMessage) {
        emailBlock.removeChild(errorMessage);
        errorMessage = null;
      }
  
      if(email.value === '' || !validateEmail(email.value)) {
        errorMessage = document.createElement('span');
        errorMessage.classList.add('text-field__message');

        if(!validateEmail(email.value)) {
          errorMessage.textContent = 'The email should contain "@" symbol and the part after "@" symbol.';
        } 
        
        if(email.value === '') {
          errorMessage.textContent = 'Field should be not empty';
        }
        
        emailBlock.appendChild(errorMessage);
        emailBlock.classList.add('error');
        submit.setAttribute('disabled', 'disabled');
        FieldFlag.email = false;
      } else {
        emailBlock.classList.remove('error');
        FieldFlag.email = true;
        
        checkAreas()
      }
    }
  
    function commentCheck() {
      FieldFirstBlur.text = true;
      var errorMessage = textBlock.querySelector(".text-field__message");

      if(errorMessage) {
        textBlock.removeChild(errorMessage);
        errorMessage = null;
      }
  
      if(text.value === '') {
        errorMessage = document.createElement('span');
        errorMessage.classList.add('text-field__message');
        errorMessage.textContent = 'Field should be not empty.';
        textBlock.appendChild(errorMessage);
        textBlock.classList.add('error');
        submit.setAttribute('disabled', 'disabled');
        FieldFlag.comment = false;
      } else {
        textBlock.classList.remove('error');
        FieldFlag.comment = true;
        
        checkAreas()
      }
    }
  
    function submitCheck() {
      if(FieldFlag.name && FieldFlag.email && FieldFlag.comment) {
        submit.removeAttribute('disabled');
        return true;
      } else {
        submit.setAttribute('disabled', 'disabled');
        return false;
      }
    }

    subject.addEventListener('blur', subjectCheck);
    last_name.addEventListener('blur', lastNameCheck);
    name.addEventListener('blur', nameCheck);
    email.addEventListener('blur', emailCheck);
    text.addEventListener('blur', commentCheck);
    form.addEventListener('submit', function(event) {
      event.preventDefault();
      nameCheck();
      emailCheck();
      commentCheck();
      subjectCheck();
      lastNameCheck();
  
      var isFormValid = submitCheck();
      if(isFormValid) {
        form.submit();
      }
    });
  }
})();