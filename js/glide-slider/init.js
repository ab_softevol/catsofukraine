if(document.addEventListener){
  document.addEventListener("DOMContentLoaded", function(){
    if(window.glide) {
      var glide = document.querySelector('.glide');

      // add controls.
    var controlsContainer = document.createElement('div');
    controlsContainer.setAttribute('data-glide-el', 'controls');
    controlsContainer.setAttribute('class', 'glide__controlls');
    
    var controlPrev = document.createElement('button');
    controlPrev.setAttribute('type', 'button');
    controlPrev.className = 'glide__arrow glide__arrow--prev';
    controlPrev.setAttribute('data-glide-dir', '<');
    controlPrev.setAttribute('aria-label', 'previous slide');
    
    var controlNext = document.createElement('button');
    controlNext.setAttribute('type', 'button');
    controlNext.className = 'glide__arrow glide__arrow--next';
    controlNext.setAttribute('data-glide-dir', '>');
    controlNext.setAttribute('aria-label', 'next slide');
    controlsContainer.appendChild(controlPrev);
    controlsContainer.appendChild(controlNext);
    glide.appendChild(controlsContainer);
  
      var glide = new Glide('.glide', {
        type: 'carousel',
        focusAt: 'center',
        startAt: 1,
        perView: 3,
        animationDuration: 500,
        autoplay: 7000,
        hoverpause: true,
        gap: 0,
        breakpoints: {
          839: {
            perView: 2
          },
          599: {
            perView: 1
          }
        }
      })
      
      glide.mount();
    }
  });
}

