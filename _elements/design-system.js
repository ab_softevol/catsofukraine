const responsive = require("./responsive.js");

function assets_block(src, width, height, alt, addClass = '') {
  if(addClass){
    addClass = " assets-block--" + addClass;
  }

  let output = `
  ${responsive.container.call(this, "100_50_33_33_33")}
    <div class="assets-block${addClass}">
      <div class="assets-block__inner">
        <div class="assets-block__buffer"><img src="${src}" width="${width}" height="${height}" alt="${alt}">
        </div>
      </div>
    </div>
  ${responsive.end_container.call(this)}`;
  return output;
}

function color_block(theme, color, label) {

  let color_block = `<div class="color-block">
                <div style="background-color: ${ color }; color: ${ label };">
                  <div class="color-block__buffer">
                    <p class="color-block__label">
                      ${ theme }
                    </p>
                    <div class="spacer-40"></div>
                    <p class="color-block__hex">
                      ${ color }
                    </p>
                  </div>
                </div>
              </div>
  `;
  return color_block;
}

function ui_kit_block(data, type, addClass = '') {
  if(addClass){
    addClass = " ui-kit-block--" + addClass;
  }

  if(!type) {
    type = '';
  } else {
    type = ' ' + type;
  }

  const template = `${responsive.container.call(this, "100_50_33_33_33")}<div class="ui-kit-block${type}${addClass}"><div class="ui-kit-block__inner"><div class="ui-kit-block__buffer">${data}</div></div></div>${responsive.end_container.call(this)}`;
  return template;
}

module.exports = {
  assets_block,
  color_block,
  ui_kit_block
}