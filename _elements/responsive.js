const utils = require("./utils.js");
const bufferData = require("../_data/buffer.json");
const containersData = require("../_data/containers.json");
const maxWidthData = require("../_data/max_width.json");

function container(params, tag, addClass = '') {
  if(!tag) {
    tag = 'div';
  }

  if(addClass) {
    addClass = ' ' + addClass
  }

  utils.pushAdaptiveOnWidthStack(this.page, containersData[params]);
  return `<${tag} class="container-${params}${addClass}">`;
}

function end_container(tag) {
  if(!tag) {
    tag = 'div';
  }
  utils.popFromWidthStack(this.page);
  return `</${tag}>`;
}

function buffer(params, addClass = '') {
  if(addClass) {
    addClass = ' ' + addClass
  }

  let bufferType = '';
  
  if(!params) {
    utils.pushGapOnWidthStack(this.page, bufferData.default);
    bufferType = 'buffer-default';
  } else {
    utils.pushGapOnWidthStack(this.page, bufferData[params]);
    bufferType = 'buffer-' + params;
  }
  return `<div class="${bufferType}${addClass}">`;
}

function end_buffer() {
  utils.popFromWidthStack(this.page);
  return `</div>`;
}

function responsive_container(content, tag, addClass = '') {
  if(!tag) {
    tag = 'div';
  }

  if(addClass) {
    addClass = ' ' + addClass
  }

  return `<${tag} class="responsive-blocks-container${addClass}">${content}</${tag}>`
}

function max_width(params, addClass = '') {

  if(!params) {
    utils.pushMaxWidthOnWidthStack(this.page, maxWidthData["default"].max_width);
    utils.pushGapOnWidthStack(this.page,  maxWidthData["default"].gap);
    params = 'default';
  } else {
    utils.pushMaxWidthOnWidthStack(this.page, maxWidthData[params].max_width);
    utils.pushGapOnWidthStack(this.page,  maxWidthData[params].gap);
    maxWidthType = 'max-width-' + params;
  }

  if(addClass) {
    addClass = ' ' + addClass
  }

  return `<div class="max-width-${params}${addClass}">`;
}

function end_max_width() {
  utils.popFromWidthStack(this.page); // padding
  utils.popFromWidthStack(this.page); // max-width
  return `</div>`;
}

module.exports = {
  container,
  end_container,
  buffer,
  end_buffer,
  responsive_container,
  max_width,
  end_max_width
}