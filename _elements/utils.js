const fs = require('fs');
const { resolve } = require('path');
const { promisify } = require('util');
const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);

function debug(obj) {
  return `<pre><code>${JSON.stringify(obj, null, 2)}</code></pre>`
}

async function getFiles(dir) {
  const subdirs = await readdir(dir);
  const files = await Promise.all(subdirs.map(async (subdir) => {
    const res = resolve(dir, subdir);
    return (await stat(res)).isDirectory() ? getFiles(res) : res;
  }));
  return files.reduce((a, f) => a.concat(f), []);
}

function compareNumeric(a, b) {
  if (a > b) return 1;
  if (a == b) return 0;
  if (a < b) return -1;
}

function getWidthStack(page) {
  if (!page.screenWidthStack) {
    page.screenWidthStack = [];
  }
  return page.screenWidthStack;
}

function pushOnWidthStack(page, params) {
  let stack = getWidthStack(page);
  stack.push(params);
}

function popFromWidthStack(page) {
  let stack = getWidthStack(page);
  if (stack.length) {
      stack.length--;
  }
}

function pushMaxWidthOnWidthStack(page, maxWidth) {
  pushOnWidthStack(page, {maxWidth: maxWidth});
}

function pushAdaptiveOnWidthStack(page, coefficients) {
  pushOnWidthStack(page, { adaptive: coefficients });
}

function pushGapOnWidthStack(page, unusedSpaceWidth) {
  if(typeof(unusedSpaceWidth) === 'object') {
    let obj = JSON.parse(JSON.stringify(unusedSpaceWidth)); 
    for (let key in obj) {
      if(key === 'padding') {
        obj[key] *= 2;
      } else if (key === 'phone' || key === 'tablet' || key === 'laptop' || key === 'desktop') {
        obj[key]["padding"] *= 2;
      }
    }
    pushOnWidthStack(page, {gap: obj});
  }
}

module.exports = {
  debug,
  getFiles,
  compareNumeric,
  getWidthStack,
  pushOnWidthStack,
  popFromWidthStack,
  pushMaxWidthOnWidthStack,
  pushAdaptiveOnWidthStack,
  pushGapOnWidthStack
}