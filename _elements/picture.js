const path = require('path');
const Image = require("@11ty/eleventy-img");

const responsiveData = require("../_data/responsive.json");
const MEDIA = responsiveData.MEDIA;
const SCREENS = responsiveData.SCREEN_WIDTHES;

const utils = require("./utils");


function calcWidthes(media, operations) {
  // console.log('operations', JSON.stringify(operations,null,2));

  let localWidthes = SCREENS.slice();
  let maxWidthArray = operations.filter(op => op.maxWidth);
  let maxWidth = localWidthes[localWidthes.length - 1];

  if(maxWidthArray.length > 0 && maxWidthArray.length <= 1) {
    maxWidth = maxWidthArray[0].maxWidth;
  } else if ( maxWidthArray.length > 1){
    maxWidth = maxWidthArray.reduce(function(res, obj) {
      return (obj.maxWidth < res.maxWidth) ? obj.maxWidth : res.maxWidth;
    });
  }

  if(maxWidth) {
    let i = 0;
    while (i < operations.length) {
      if(Object.keys(operations[i])[0] === 'gap') {
        maxWidth += operations[i].gap.padding;
      }
      if(Object.keys(operations[i])[0] === 'maxWidth') {
        if(i === 0) {
          for(let i = 0; i < localWidthes.length; i++) {
            if(localWidthes[i] >= maxWidth) {
              localWidthes[i] = maxWidth;
              localWidthes = localWidthes.slice(0, i+1);
            }
          }
        }
        break;
      }
      i++;
    }
  }

  let mediaRange = 0;

  for(width of localWidthes) {
    if(width < 768) {
      mediaRange++;
    }
  }

  let localWidthesProcessed = localWidthes.slice();

  for(let i = 0; i < localWidthesProcessed.length; i++) {
    for(let op of operations) {    
      if(op.gap) {

        let gap = 0;
        if(typeof(op.gap) === 'object') {
          if(localWidthes[i] < media.phone) {
            gap = op.gap.padding;
          } else if(localWidthes[i] < media.tablet && typeof op.gap['phone'] !== "undefined") {
            gap = op.gap.phone.padding;
          } else if(localWidthes[i] < media.laptop && typeof op.gap['tablet'] !== "undefined") {
            gap = op.gap.tablet.padding;
          } else if(localWidthes[i] < media.desktop && typeof op.gap['laptop'] !== "undefined") {
            gap = op.gap.laptop.padding;
          } else if(typeof op.gap['desktop'] !== "undefined") {
            gap = op.gap.desktop.padding;
          } else {
            gap = op.gap.padding;
          }
        }

        localWidthesProcessed[i] -= gap;
      }
  
      if(op.adaptive) {
        if(localWidthes[i] < media.phone) {
          localWidthesProcessed[i] *= op.adaptive.default;
        } else if(localWidthes[i] < media.tablet) {
          localWidthesProcessed[i] *= op.adaptive.phone;
        } else if(localWidthes[i] < media.laptop) {
          localWidthesProcessed[i] *= op.adaptive.tablet;
        } else if(localWidthes[i] < media.desktop) {
          localWidthesProcessed[i] *= op.adaptive.laptop;
        } else {
          localWidthesProcessed[i] *= op.adaptive.desktop;
        }
        localWidthesProcessed[i] = Math.round(localWidthesProcessed[i]);
      }
    }
  }

  maxWidth = localWidthesProcessed[localWidthesProcessed.length - 1];
  let count = localWidthesProcessed.length;

  for(let i = 0; i < count; i++) {
    let width = localWidthesProcessed[i];
    if(i < mediaRange) {
      localWidthesProcessed.push(width*2);
      localWidthesProcessed.push(width*3);
    } else {
      localWidthesProcessed.push(Math.round(width*1.5));
      localWidthesProcessed.push(width*2);
    }
  }

  localWidthesProcessed.sort(utils.compareNumeric);
  // Внутренняя неочевидная логика. Функция всегда выдает последним элементом массива максимальную ширину картинки для фолбека (без коэффициента ретины). 
  // Позже, на основе последнего элемента в функции picture будет нарезан png/jpg фолллбэк для старых браузеров.
  let set = Array.from(new Set(localWidthesProcessed));
  set.splice(set.indexOf(maxWidth), 1);
  set.push(maxWidth);

  return set;
}

function calcSizes(media, operations) {
  let sizesDefault = '100vw';
  let maxWidth = null;
  let sizes = {
    phone: sizesDefault,
    tablet: sizesDefault,
    laptop: sizesDefault,
    desktop: sizesDefault,
    default: sizesDefault
  };

  for(let i = 0; i < operations.length; i++) {
    if(operations[i].maxWidth) {
      maxWidth = operations[i].maxWidth;
      sizes['maxWidth'] = operations[i].maxWidth;
    }
  }

  if(maxWidth) {
    let i = 0;
    while (i < operations.length) {
      if(Object.keys(operations[i])[0] === 'gap') {
        maxWidth += operations[i].gap.padding;
        sizes['maxWidth'] += operations[i].gap.padding;
      }

      if(Object.keys(operations[i])[0] === 'maxWidth') {
        break;
      }

      i++;
    }
  }

  for(let op of operations) {
    if(op.adaptive) {
      if(op.adaptive.default < 1) {
        sizes.default = `(${sizes.default} * ${(op.adaptive.default).toFixed(2)})`;
        if (maxWidth && maxWidth >= media.desktop) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.default));
        }
      }

      if(op.adaptive.phone < 1) {
        sizes.phone = `(${sizes.phone} * ${(op.adaptive.phone).toFixed(2)})`;
        if(maxWidth && maxWidth < media.phone) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.phone));
        }
      }

      if(op.adaptive.tablet < 1) {
        sizes.tablet = `(${sizes.tablet} * ${(op.adaptive.tablet).toFixed(2)})`;
        if(maxWidth && maxWidth >= media.phone && maxWidth < media.tablet) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.tablet));
        }
      }
      
      if(op.adaptive.laptop < 1) {
        sizes.laptop = `(${sizes.laptop} * ${(op.adaptive.laptop).toFixed(2)})`;
        if(maxWidth && maxWidth >= media.tablet && maxWidth < media.laptop) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.laptop));
        }
      }
 
      if(op.adaptive.desktop < 1) {
        sizes.desktop = `(${sizes.desktop} * ${(op.adaptive.desktop).toFixed(2)})`;
        if(maxWidth && maxWidth >= media.laptop && maxWidth < media.desktop) {
          sizes.maxWidth = Math.round(sizes.maxWidth * (op.adaptive.desktop));
        }
      }
    }

    if(op.gap) {
      if(typeof(op.gap) === 'object') {
        sizes.default = `(${sizes.default} - ${op.gap.padding}px)`;

        if(typeof op.gap['phone'] !== "undefined") {
          sizes.phone = `(${sizes.phone} - ${op.gap.phone.padding}px)`;
        } else {
          sizes.phone = `(${sizes.phone} - ${op.gap.padding}px)`;
        }
  
        if(typeof op.gap['tablet'] !== "undefined") {
          sizes.tablet = `(${sizes.tablet} - ${op.gap.tablet.padding}px)`;
        } else {
          sizes.tablet = `(${sizes.tablet} - ${op.gap.padding}px)`;
        }
  
        if(typeof op.gap['laptop'] !== "undefined") {
          sizes.laptop = `(${sizes.laptop} - ${op.gap.laptop.padding}px)`;
        } else {
          sizes.laptop = `(${sizes.laptop} - ${op.gap.padding}px)`;
        }
  
        if(typeof op.gap['desktop'] !== "undefined") {
          sizes.desktop = `(${sizes.desktop} - ${op.gap.desktop.padding}px)`;
        } else {
          sizes.desktop = `(${sizes.desktop} - ${op.gap.padding}px)`;
        }
      }

      if(sizes.maxWidth) {
        if(maxWidth < media.phone) {
          sizes.maxWidth -= op.gap.padding;
        } else if(maxWidth < media.tablet && typeof op.gap['phone'] !== "undefined") {
          sizes.maxWidth -= op.gap.phone.padding;
        } else if(maxWidth < media.laptop && typeof op.gap['tablet'] !== "undefined") {
          sizes.maxWidth -= op.gap.tablet.padding;
        } else if(maxWidth < media.desktop && typeof op.gap['laptop'] !== "undefined") {
          sizes.maxWidth -= op.gap.laptop.padding;
        } else if(typeof op.gap['desktop'] !== "undefined") {
          sizes.maxWidth -= op.gap.desktop.padding;
        } else {
          sizes.maxWidth -= op.gap.padding;
        }
      }
    }
  }

  let result = '';

  for(let size in sizes) {
    if(size === 'maxWidth') {
      result = `(min-width: ${maxWidth}px) ${Math.round(sizes[size])}px, ` + result;
    }

    if(size  === 'default') {
      result = result + `calc(${sizes[size]})`;
    }

    if(size  === 'phone') {
      result = `(min-width: ${media.phone}px) calc(${sizes[size]}), ` + result;
    }
    
    if(size  === 'tablet') {
      result = `(min-width: ${media.tablet}px) calc(${sizes[size]}), ` + result;
    }
    
    if(size  === 'laptop') {
      result = `(min-width: ${media.laptop}px) calc(${sizes[size]}), ` + result;
    }

    if(size  === 'desktop') {
      result = `(min-width: ${media.desktop}px) calc(${sizes[size]}), ` + result;
    }
  }
  return result;
}

async function picture(src, alt) {
  const extension = path.extname(src);
  const dirname = path.dirname(src);
  const name = path.basename(src, extension);
  const outputName = dirname.replaceAll('/', '-') + '-' + name;
  let operationStack = utils.getWidthStack(this.page);

  let result = '';
  let srcset = calcWidthes(MEDIA, operationStack);
  let sizes = calcSizes(MEDIA, operationStack);

  if(extension == '.svg') {
    let metadata = await Image(src, {
      svgAllowUpscale: true,
      widths: [srcset[srcset.length - 1]],
      formats: ['png'],
      outputDir: './_site/img',
      filenameFormat: function (id, src, width, format, options) {
        return `${outputName}-${[srcset[srcset.length - 1]]}w.png`;
      }
    });

    result = `<picture>
      <source type="image/svg+xml" srcset="/${src}">
      <img src="${metadata.png[0].url}" alt="${alt}" width="${metadata.png[0].width}" height="${metadata.png[0].height}">
    </picture>`;
  } else if (extension == '.png') {
    let metadata = await Image(src, {
      widths: srcset,
      formats: ['webp'],
      outputDir: './_site/img/',
      filenameFormat: function (id, src, width, format, options) {
        return `${outputName}-${width}w.${format}`;
      }
    });

    let fallbackMetadata = await Image(src, {
      widths: [srcset[srcset.length - 1]],
      formats: ['png'],
      outputDir: './_site/img/',
      filenameFormat: function (id, src, width, format, options) {
        return `${outputName}-${width}w.${format}`;
      }
    });

      let fallbackWidth;
      let fallbackHeight;

      if(srcset[srcset.length - 1] !== fallbackMetadata.png[0].width) {
        fallbackWidth = srcset[srcset.length - 1];
        fallbackHeight = Math.round((fallbackWidth/fallbackMetadata.png[0].width)*fallbackMetadata.png[0].height);
      } else {
        fallbackWidth = fallbackMetadata.png[0].width;
        fallbackHeight = fallbackMetadata.png[0].height;
      }

      result  = 
      `<picture>
        <source sizes="${sizes}" type="image/webp" srcset="\n`;
      result += metadata['webp'].map(webp => `      ${webp.srcset}`).join(",\n");
      result += `">\n`;
      result += `<!-- TODO add img -->\n`;
      result += `  <img src="${fallbackMetadata.png[0].url}" alt="${alt}" width="${fallbackWidth}" height="${fallbackHeight}">\n`;
      result += `</picture>`;
  } else if (extension == '.jpg' || extension == '.jpeg') {
    let metadata = await Image(src, {
      widths: srcset,
      formats: ['webp'],
      outputDir: './_site/img/',
      filenameFormat: function (id, src, width, format, options) {
        return `${outputName}-${width}w.${format}`;
      }
    });
    
    let fallbackMetadata = await Image(src, {
      widths: [srcset[srcset.length - 1]],
      formats: ['jpeg'],
      outputDir: './_site/img/',
      filenameFormat: function (id, src, width, format, options) {
        return `${outputName}-${width}w.${format}`;
      }
    });

    let fallbackWidth;
    let fallbackHeight;

    if(srcset[srcset.length - 1] !== fallbackMetadata.jpeg[0].width) {
      fallbackWidth = srcset[srcset.length - 1];
      fallbackHeight = Math.round((fallbackWidth/fallbackMetadata.jpeg[0].width)*fallbackMetadata.jpeg[0].height);
    } else {
      fallbackWidth = fallbackMetadata.jpeg[0].width;
      fallbackHeight = fallbackMetadata.jpeg[0].height;
    }

    result  = 
    `<picture>
      <source sizes="${sizes}" type="image/webp" srcset="\n`;
    result += metadata['webp'].map(webp => `      ${webp.srcset}`).join(",\n");
    result += `">\n`;
    result += `<!-- TODO add img -->\n`;
    result += `  <img src="${fallbackMetadata.jpeg[0].url}" alt="${alt}" width="${fallbackWidth}" height="${fallbackHeight}">\n`;
    result += `</picture>`;
  } else {
    throw new Error('Unsupported file format');
  }
  return result;
}

async function pictureSvgPng(src, alt, width, height) {
  // console.log('src', src);
  // console.log('alt', alt);
  // console.log('width', width);
  // console.log('height', height);

  if(alt === undefined) {
      // You bet we throw an error on missing alt (alt="" works okay)
      throw new Error(`Missing \`alt\` on responsiveimage from: ${src}`);
  }
  
  let metadata = await Image(src, {
      formats: ['png', 'svg'],
      outputDir: './_site/img'
  });
  
  return `<picture>
              <source type="image/svg+xml" srcset="${metadata.svg[0].url}">
              <img src="${metadata.png[0].url}" width="${width}" height="${height}" alt="${alt}">
          </picture>`;
}

async function picturePngWebp(src, alt, width, height) {
  if(alt === undefined) {
      // You bet we throw an error on missing alt (alt="" works okay)
      throw new Error(`Missing \`alt\` on responsiveimage from: ${src}`);
  }

  let localWidths = [];
  localWidths.push(width*1.0)
  localWidths.push(width*1.5)
  localWidths.push(width*2)
  localWidths.push(width*3)
  localWidths = Array.from(new Set(localWidths));
  
  let metadata = await Image(src, {
      widths: localWidths,
      formats: ['png', 'webp'],
      outputDir: './_site/img'
  });

  let result = `<picture>
    <source sizes="${width}px" type="image/webp" srcset="\n`;
  result += metadata['webp'].map(webp => `      ${webp.srcset}`).join(",\n");
  result += `">\n`;
  result += `  <img src="${metadata.png[0].url}" alt="${alt}" width="${width}" height="${height}">\n`;
  result += `</picture>`;
  
  return result;
}

async function imagePngSelfSize(src, alt, sizes  = "100vw", width, height) {
  if(alt === undefined) {
      // You bet we throw an error on missing alt (alt="" works okay)
      throw new Error(`Missing \`alt\` on responsiveimage from: ${src}`);
  }

  let x1 = width;
  let x15 = width * 1.5;
  let x2 = width * 2;
  let x3 = width * 3;

  let imgWidthes = [x1, x15, x2, x3];
  // let imgWidthes = [x1];
  
  let metadata = await Image(src, {
      widths: imgWidthes,
      formats: ['webp', 'png'],
      outputDir: './_site/assets/img'
  });
  
  let lowsrc = metadata.png[0];
  
  return `<picture>
  ${Object.values(metadata).map(imageFormat => {
    return `              <source type="${imageFormat[0].sourceType}" srcset="/assets${imageFormat.map(entry => entry.srcset).join(",\n /assets")}" sizes="${sizes}">`;    }).join("\n")}
                  <img
                      src="/assets${lowsrc.url.substring(0,(lowsrc.url.lastIndexOf('-')+1))+width}.png"
                      width="${width}"
                      height="${height}"
                      alt="${alt}"
                      loading="lazy"
                      decoding="async">
            </picture>
  `;
}

module.exports = {
  picture,
  pictureSvgPng,
  picturePngWebp,
  imagePngSelfSize
}