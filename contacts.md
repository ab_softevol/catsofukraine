---
layout: base.njk
title:  Contnact Us
---

{% article_hero %}
<div class="spacer spacer-50"></div>

# {{title}}

<div class="spacer spacer-100"></div>
{% endarticle_hero %}

<div class="contact-us contact-us_page">
  <div class="spacer spacer-50"></div>
  <section>
    {% max_width %}
      {% buffer %}
        <div class="contact-us_container">
          <div class="spacer spacer-30"></div>
          {% responsive_container %}
            {% container "100_50_50_50_50" %}
              {% buffer %}
                <h2>Heading</h2>
                <p>The cat is similar in anatomy to the other felid species: it has a strong flexible body, quick reflexes, sharp teeth and retractable claws adapted to killing small prey. Its night vision and sense of smell are well developed.</p>
                <div class="spacer spacer-10"></div>
                {% buffer %}
                  <button class="button-colored button-colored--green" >start chat
                  {% pictureSvgPng "img/icons/whatsapp.svg", "", "24", "24" %}
                  </button>
                {% end_buffer %}
                {% buffer %}
                  <button class="button-colored button-colored--blue">start chat
                  {% pictureSvgPng "img/icons/telegram.svg", "", "24", "24" %}
                  </button>
                {% end_buffer %}
              {% end_buffer %}
            {% end_container %}
            {% container "100_50_50_50_50" %}
              {% buffer %}
                <h2>Contact Us</h2>
                {% include 'partials/contact-form.njk' %}
              {% end_buffer %}
            {% end_container %}
          {% endresponsive_container %}
          <div class="spacer spacer-30"></div>
        </div>  
      {% end_buffer %}
    {% end_max_width %}
  </section>
<div class="spacer spacer-50"></div>
</div>